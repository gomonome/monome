module gitlab.com/gomonome/monome

require (
	github.com/karalabe/gousb v0.0.0-20170124160949-ffa821b2e25a
	gitlab.com/goosc/osc v0.2.0
	gitlab.com/metakeule/config v1.13.0
)
